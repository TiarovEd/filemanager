﻿using filemanager.DtoModels;
using System;
using System.Windows.Media.Imaging;
using System.Windows;

namespace filemanager
{
    /// <summary>
    /// Interaction logic for PropertiesWindow.xaml
    /// </summary>
    public partial class PropertiesWindow : Window
    {
        private const int scale = 1024;

        public PropertiesWindow(FilePropertiesDto file)
        {
            InitializeComponent();
            SetPropOfFile(file);
        }

        public PropertiesWindow(FolderPropertiesDto folder)
        {
            InitializeComponent();
            SetPropOfFolder(folder);
        }

        public PropertiesWindow(DrivePropertiesDto drive)
        {
            InitializeComponent();
            SetPropOfDrive(drive);
        }

        private void SetCommonProp(ObjInFileSystemPropertiesDto objDto)
        {
            Name.Text = "Name: " + objDto.Name;
            Path.Text = "Path: " + objDto.Path;
            var icon = new BitmapImage();
            icon.BeginInit();
            icon.UriSource = new Uri("pack://application:,,,/filemanager;component/" + objDto.PathToIcon);
            icon.EndInit();
            Icon.Source = icon;
        }

        private void SetPropOfFile(FilePropertiesDto file)
        {
            FileFolderStackPanel.Visibility = Visibility.Visible;
            DriveStackPanel.Visibility = Visibility.Collapsed;

            SetCommonProp(file);

            TypeOfFile.Text = "Type : " + file.Type;
            DateOfCreate.Text = "Created : " + file.DateOfCreate;
            DateOfChanged.Text = "Modified : " + file.DateOfChange;
            DateOfLastAccess.Text = "Accessed : " + file.DateOfLastAccess;
            SizeInMb.Text = String.Format("Size : {0} megabytes", Math.Round(file.Size / (Math.Pow(scale, 2)), 2));
            Atributes.Text = "Attributes : " + file.Atributes;
        }

        private void SetPropOfFolder(FolderPropertiesDto folder)
        {
            FileFolderStackPanel.Visibility = Visibility.Visible;
            DriveStackPanel.Visibility = Visibility.Collapsed;

            SetCommonProp(folder);

            TypeOfFile.Text = "Type : File folder";
            DateOfCreate.Text = "Created : " + folder.DateOfCreate;
            DateOfChanged.Text = "Modified : " + folder.DateOfChange;
            DateOfLastAccess.Text = "Accessed : " + folder.DateOfLastAccess;
            SizeInMb.Text = String.Format("Size : {0} megabytes", Math.Round(folder.Size / (Math.Pow(scale, 2)), 2));
            Atributes.Text = "Attributes : " + folder.Atributes;
        }

        private void SetPropOfDrive(DrivePropertiesDto drive)
        {
            FileFolderStackPanel.Visibility = Visibility.Collapsed;
            DriveStackPanel.Visibility = Visibility.Visible;

            SetCommonProp(drive);

            if (String.IsNullOrWhiteSpace(drive.TypeOfDrive))
                TypeOfDrive.Visibility = Visibility.Collapsed;
            else
                TypeOfDrive.Text = "Type : " + drive.TypeOfDrive;
            FormatOfDrive.Text = "File system : " + drive.FormatOfDrive;
            TotalSize.Text = String.Format("Total Size : {0}", FormatSizeToGb(drive.TotalSize));
            FreeSpace.Text = String.Format("Free Space : {0}", FormatSizeToGb(drive.FreeSpace));
            BusySpace.Text = String.Format("Busy Space : {0}", FormatSizeToGb(drive.BusySpace));
        }

        private string FormatSizeToGb(long x)
        {
            return String.Format("{0} gb", (Math.Round(x / (Math.Pow(scale, 3)), 2).ToString()));
        }
    }
}
