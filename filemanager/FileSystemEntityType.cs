﻿
namespace filemanager
{
    public enum FileSystemEntityType
    {
        Drive,
        Folder,
        File
    }
}
