﻿
namespace filemanager
{
    public class InputDialogResult
    {
        public InputDialogResult(InputBoxResult click, string input)
        {
            ClickedButton = click;
            UserInput = input;
        }

        public InputBoxResult ClickedButton { get; private set; }
        public string UserInput { get; private set; }
    }
}
