﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using System.Text;
using System;
using filemanager.Models;
using filemanager.ViewModels;
using filemanager.DtoModels;

namespace filemanager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string FormatForDataObject = "FileSystemItem";
        private const string InternalErrorMessage = "Internal error!";

        private readonly FileProvider _fileProvider = new FileProvider();
        private readonly FileManager _fileManager = new FileManager();
        private readonly VmBuilder _viewModelBuilder = new VmBuilder();

        private Point startPoint;

        private ContextMenu CommonContextMenu
        {
            get
            {
                var menu = Resources["CommonContextMenu"] as ContextMenu;
                if (menu == null)
                    throw new Exception("Internal error!");
                return menu;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                LeftFolderAndFileListView.ContextMenu = CommonContextMenu;
                RightFolderAndFileListView.ContextMenu = CommonContextMenu;
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
                Close();
            }
            ShowHomeDir(LeftFolderAndFileListView, LeftHiddenAddressBarTextBox);
            ShowHomeDir(RightFolderAndFileListView, RightHiddenAddressBarTextBox);
        }

        #region Handlers of Left Part

        private void LeftUpButton_Click(object sender, RoutedEventArgs e)
        {
            CancelAction(LeftAddressBarTextBox, LeftHiddenAddressBarTextBox, LeftFolderAndFileListView, LeftStatusBarTextBlock);
        }

        private void LeftHiddenAddressBarTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetPathToAddressBar(LeftAddressBarTextBox, LeftHiddenAddressBarTextBox.Text, LeftStatusBarTextBlock);
            SetCurrentDirToHiddenContainer(LeftHiddenAddressBarTextBox.Text, LeftContainerForModelLabel);
        }

        private void LeftAddressBarTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            LeftAddressBarTextBox.Text = LeftHiddenAddressBarTextBox.Text;
        }

        private void LeftAddressBarTextBox_KeyEnterDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CheckAndUpdateListView(LeftAddressBarTextBox, LeftHiddenAddressBarTextBox, LeftFolderAndFileListView);
            }
        }

        private void LeftSwipeButton_Click(object sender, RoutedEventArgs e)
        {
            ShowContentInListViewByPath(LeftAddressBarTextBox, LeftHiddenAddressBarTextBox, LeftFolderAndFileListView, RightHiddenAddressBarTextBox.Text);
        }

        private void LeftSearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchAndShowFolderAndFilesInCurrentDir(LeftHiddenAddressBarTextBox.Text, LeftAddressBarTextBox, LeftStatusBarTextBlock, LeftSearchTextBox.Text, LeftFolderAndFileListView);
            }
        }

        private void LeftSearchButton_Click(object sender, RoutedEventArgs e)
        {
            SearchAndShowFolderAndFilesInCurrentDir(LeftHiddenAddressBarTextBox.Text, LeftAddressBarTextBox, LeftStatusBarTextBlock, LeftSearchTextBox.Text, LeftFolderAndFileListView);
        }

        private void LeftUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateList(LeftFolderAndFileListView, LeftHiddenAddressBarTextBox, LeftAddressBarTextBox, LeftStatusBarTextBlock);
        }

        private void LeftFolderAndFileListView_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            var listView = sender as ListView;
            if (listView == null)
                return;

            bool isHomeDir = String.IsNullOrWhiteSpace(LeftAddressBarTextBox.Text);
            var originalSourse = e.OriginalSource as ScrollViewer;
            if (originalSourse == null)
                return;

            if (isHomeDir && Object.ReferenceEquals(originalSourse.TemplatedParent, sender))
            {
                e.Handled = true;
            }
            SetPointsOfContextMenu(listView.ContextMenu);
        }

        private void LeftFolderAndFileListView_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            DoDragItem(sender, e, LeftAddressBarTextBox.Text);
        }

        private void LeftFolderAndFileListView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        private void LeftFolderAndFileListView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = ResolveMoveDragEffects(sender, e, LeftAddressBarTextBox.Text);
        }

        private void LeftFolderAndFileListView_Drop(object sender, DragEventArgs e)
        {
            DoDropItem(sender, e, LeftAddressBarTextBox.Text);
        }

        #endregion

        #region Handlers of Right Part

        private void RightUpButton_Click(object sender, RoutedEventArgs e)
        {
            CancelAction(RightAddressBarTextBox, RightHiddenAddressBarTextBox, RightFolderAndFileListView, RightStatusBarTextBlock);
        }

        private void RightHiddenAddressBarTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetPathToAddressBar(RightAddressBarTextBox, RightHiddenAddressBarTextBox.Text, RightStatusBarTextBlock);
            SetCurrentDirToHiddenContainer(RightHiddenAddressBarTextBox.Text, RightContainerForModelLabel);
        }

        private void RightAddressBarTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            RightAddressBarTextBox.Text = RightHiddenAddressBarTextBox.Text;
        }

        private void RightAddressBarTextBox_KeyEnterDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CheckAndUpdateListView(RightAddressBarTextBox, RightHiddenAddressBarTextBox, RightFolderAndFileListView);
            }
        }

        private void RightSwipeButton_Click(object sender, RoutedEventArgs e)
        {
            ShowContentInListViewByPath(RightAddressBarTextBox, RightHiddenAddressBarTextBox, RightFolderAndFileListView, LeftHiddenAddressBarTextBox.Text);
        }

        private void RightSearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchAndShowFolderAndFilesInCurrentDir(RightHiddenAddressBarTextBox.Text, RightAddressBarTextBox, RightStatusBarTextBlock, RightSearchTextBox.Text, RightFolderAndFileListView);
            }
        }

        private void RightSearchButton_Click(object sender, RoutedEventArgs e)
        {
            SearchAndShowFolderAndFilesInCurrentDir(RightHiddenAddressBarTextBox.Text, RightAddressBarTextBox, RightStatusBarTextBlock, RightSearchTextBox.Text, RightFolderAndFileListView);
        }

        private void RightUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateList(RightFolderAndFileListView, RightHiddenAddressBarTextBox, RightAddressBarTextBox, RightStatusBarTextBlock);
        }

        private void RightFolderAndFileListView_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            var listView = sender as ListView;
            if (listView == null)
                return;

            bool isHomeDir = String.IsNullOrWhiteSpace(RightAddressBarTextBox.Text);
            var originalSourse = e.OriginalSource as ScrollViewer;

            if (originalSourse == null)
                return;

            if (isHomeDir && Object.ReferenceEquals(originalSourse.TemplatedParent, sender))
            {
                e.Handled = true;
            }
            SetPointsOfContextMenu(listView.ContextMenu);
        }

        private void RightFolderAndFileListView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        private void RightFolderAndFileListView_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            DoDragItem(sender, e, RightAddressBarTextBox.Text);
        }

        private void RightFolderAndFileListView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = ResolveMoveDragEffects(sender, e, RightAddressBarTextBox.Text);
        }

        private void RightFolderAndFileListView_Drop(object sender, DragEventArgs e)
        {
            DoDropItem(sender, e, RightAddressBarTextBox.Text);
        }

        #endregion

        #region Common Handlers

        private void Window_LeftContralAndRightOrLeftKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right && e.KeyboardDevice.IsKeyDown(Key.LeftCtrl))
                ShowContentInListViewByPath(RightAddressBarTextBox, RightHiddenAddressBarTextBox, RightFolderAndFileListView, LeftHiddenAddressBarTextBox.Text);
            if (e.Key == Key.Left && e.KeyboardDevice.IsKeyDown(Key.LeftCtrl))
                ShowContentInListViewByPath(LeftAddressBarTextBox, LeftHiddenAddressBarTextBox, LeftFolderAndFileListView, RightHiddenAddressBarTextBox.Text);
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = GetViewModelFromObjectOfListVeiewItem(sender);
            if (selectedItem == null)
                return;

            if (selectedItem.Position == WindowPosition.Left)
            {
                ResolveSelectedItem(selectedItem, LeftFolderAndFileListView, LeftHiddenAddressBarTextBox);

                return;
            }
            if (selectedItem.Position == WindowPosition.Right)
            {
                ResolveSelectedItem(selectedItem, RightFolderAndFileListView, RightHiddenAddressBarTextBox);

                return;
            }
        }

        private void ListViewItem_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            var listViewItem = sender as ListViewItem;
            if (sender == null)
                return;

            var selectedItem = listViewItem.Content as ObjectInFileSystemViewModel;
            if (selectedItem == null)
                return;

            if (selectedItem.Position == WindowPosition.Left)
                SetPointsOfItemsContextMenu(LeftAddressBarTextBox.Text, listViewItem.ContextMenu);
            if (selectedItem.Position == WindowPosition.Right)
                SetPointsOfItemsContextMenu(RightAddressBarTextBox.Text, listViewItem.ContextMenu);
        }

        private void PropertiesCommonMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var list = GetListViewFromObjectOfMenuItem(sender);
            if (list == null)
                return;

            if (list == LeftFolderAndFileListView)
                ShowProperties(null, LeftHiddenAddressBarTextBox.Text, LeftContainerForModelLabel);
            else if (list == RightFolderAndFileListView)
                ShowProperties(null, RightHiddenAddressBarTextBox.Text, RightContainerForModelLabel);
        }

        private void PropertiesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = GetViewModelFromObjectOfMenuItem(sender);
            if (item == null)
                return;

            if (item.Position == WindowPosition.Left)
            {
                ShowProperties(item, LeftHiddenAddressBarTextBox.Text, LeftContainerForModelLabel);
            }
            else if (item.Position == WindowPosition.Right)
            {
                ShowProperties(item, RightHiddenAddressBarTextBox.Text, RightContainerForModelLabel);
            }
        }

        private void CutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = GetViewModelFromObjectOfMenuItem(sender);
            if (item == null)
                return;

            SelectItemInListView(item, true);
        }

        private void CopyMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = GetViewModelFromObjectOfMenuItem(sender);
            if (item == null)
                return;

            SelectItemInListView(item, false);
        }

        private void PasteItem_Click(object sender, RoutedEventArgs e)
        {
            WindowPosition position = ResolveObjectAndGetPosition(sender);

            if (position == WindowPosition.Left)
            {
                PasteItem(LeftHiddenAddressBarTextBox.Text);
            }
            else if (position == WindowPosition.Right)
            {
                PasteItem(RightHiddenAddressBarTextBox.Text);
            }
            UpdateListViews();
        }

        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = GetViewModelFromObjectOfMenuItem(sender);
            if (item == null)
                return;

            DeleteItem(item);

            UpdateListViews();
        }

        private void RenameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var item = GetViewModelFromObjectOfMenuItem(sender);
            if (item == null)
                return;

            RenameItem(item);

            UpdateListViews();
        }

        #endregion

        #region Methods for Update, UP Butttons, Swing

        private void UpdateList(ListView navigListView, TextBox hiddenAddressBar, TextBox addressBar, TextBlock statusBar)
        {
            MakeAddressBarVisible(addressBar, statusBar);
            if (hiddenAddressBar.Text != String.Empty)
                ShowContentInDirByPath(hiddenAddressBar.Text, navigListView, hiddenAddressBar);
            else
                ShowHomeDir(navigListView, hiddenAddressBar);
        }

        private void CheckAndUpdateListView(TextBox addressBar, TextBox hiddenAddressBar, ListView navigListView)
        {
            if (String.IsNullOrWhiteSpace(addressBar.Text))
            {
                ShowHomeDir(navigListView, hiddenAddressBar);
            }
            else if (_fileProvider.CheckValidPathToDir(addressBar.Text))
            {
                ShowContentInDirByPath(addressBar.Text, navigListView, hiddenAddressBar);
            }
            else
            {
                if (!_fileProvider.TryOpenFileByPath(addressBar.Text))
                    MessageBox.Show("Incorret Input!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                addressBar.Text = hiddenAddressBar.Text;
            }
        }

        private void CancelAction(TextBox addresBarTextBox, TextBox hiddenAddressBar, ListView navigListView, TextBlock statusBarTextBlock)
        {
            if (statusBarTextBlock.Visibility == Visibility.Visible)
            {
                MakeAddressBarVisible(addresBarTextBox, statusBarTextBlock);

                ShowContentInDirByPath(hiddenAddressBar.Text, navigListView, hiddenAddressBar);
            }
            else
            {
                bool isDrive = true;

                try
                {
                    isDrive = _fileProvider.CheckIsDrive(hiddenAddressBar.Text);
                }
                catch (Exception ex)
                {
                    ShowMessageByEx(ex);
                    return;
                }

                if (isDrive || hiddenAddressBar.Text == String.Empty)
                {
                    ShowHomeDir(navigListView, hiddenAddressBar);
                }
                else
                {
                    var dir = _fileProvider.GetParentByPath(hiddenAddressBar.Text);

                    if (dir is Folder)
                    {
                        Folder folder = (Folder)dir;
                        FolderViewModel folderVm = _viewModelBuilder.CreateVmOfFolder(folder);
                        folderVm.Position = GetWindowPositionOfItem(navigListView);
                        ResolveSelectedItem(folderVm, navigListView, hiddenAddressBar);
                    }
                    if (dir is Drive)
                    {
                        Drive drive = (Drive)dir;
                        DriveViewModel driveVm = _viewModelBuilder.CreateVmOfDrive(drive);

                        ResolveSelectedItem(driveVm, navigListView, hiddenAddressBar);
                    }
                }
            }
        }

        private void ShowContentInListViewByPath(TextBox addressBar, TextBox hiddenAddressBar, ListView navigListView, string newPath)
        {
            addressBar.Text = newPath;
            CheckAndUpdateListView(addressBar, hiddenAddressBar, navigListView);
        }

        #endregion

        #region Methods for ListView

        private void ResolveSelectedItem(object selectedItem, ListView navigListView, TextBox addressBarTextBox)
        {
            if ((selectedItem is FolderViewModel) || (selectedItem is DriveViewModel))
            {
                ObjectInFileSystemViewModel currentDirVm = (ObjectInFileSystemViewModel)selectedItem;
                ShowContentInDirByPath(currentDirVm.Path, navigListView, addressBarTextBox);
            }
            else if (selectedItem is FileViewModel)
            {
                FileViewModel currentFile = (FileViewModel)selectedItem;

                if (!_fileProvider.TryOpenFileByPath(currentFile.Path))
                {
                    MessageBox.Show("Error occured while trying open file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ShowContentInDirByPath(string path, ListView navigListView, TextBox hiddenAddressBarTextBox)
        {
            List<Folder> listOfFolders;

            try
            {
                listOfFolders = _fileProvider.GetListOfFolders(path);
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
                return;
            }

            navigListView.Items.Clear();

            foreach (var folder in listOfFolders)
            {
                var folderVm = _viewModelBuilder.CreateVmOfFolder(folder);
                folderVm.Position = GetWindowPositionOfItem(navigListView);

                navigListView.Items.Add(folderVm);
            }


            List<File> listOfFiles;

            try
            {
                listOfFiles = _fileProvider.GetListOfFiles(path);
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
                return;
            }

            foreach (var file in listOfFiles)
            {
                var fileVm = _viewModelBuilder.CreateVmOfFile(file);
                fileVm.Position = GetWindowPositionOfItem(navigListView);

                navigListView.Items.Add(fileVm);
            }

            hiddenAddressBarTextBox.Text = path;
        }

        private void ShowHomeDir(ListView list, TextBox addressBarTextBox)
        {
            List<Drive> listOfDrivers = null;

            try
            {
                listOfDrivers = _fileProvider.GetListOfDrives();
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
                return;
            }

            list.Items.Clear();
            foreach (Drive drive in listOfDrivers)
            {
                var driveVm = _viewModelBuilder.CreateVmOfDrive(drive);
                driveVm.Position = GetWindowPositionOfItem(list);

                list.Items.Add(driveVm);
            }
            addressBarTextBox.Text = String.Empty;
        }

        private void UpdateListViews()
        {
            UpdateList(LeftFolderAndFileListView, LeftHiddenAddressBarTextBox, LeftAddressBarTextBox, LeftStatusBarTextBlock);
            UpdateList(RightFolderAndFileListView, RightHiddenAddressBarTextBox, RightAddressBarTextBox, RightStatusBarTextBlock);
        }

        private WindowPosition GetWindowPositionOfItem(ListView navigListView)
        {
            if (navigListView == LeftFolderAndFileListView)
                return WindowPosition.Left;
            if (navigListView == RightFolderAndFileListView)
                return WindowPosition.Right;

            throw new Exception();
        }

        #endregion

        #region Methods for addressBars and containers

        private void SetCurrentDirToHiddenContainer(string path, Label container)
        {
            try
            {
                if (_fileProvider.CheckIsDrive(path))
                {
                    container.Content = _fileProvider.GetDriveByPath(path);
                }
                else if (_fileProvider.CheckValidPathToDir(path))
                {
                    container.Content = _fileProvider.GetFolderByPath(path);
                }
                else
                {
                    container.Content = null;
                }
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
            }
        }

        private void SetPathToAddressBar(TextBox addressBarTextBox, string path, TextBlock statusBarTextBlock)
        {
            MakeAddressBarVisible(addressBarTextBox, statusBarTextBlock);
            addressBarTextBox.Text = path;
        }

        #endregion

        #region Methods for SearchStatus and Search

        private void SearchAndShowFolderAndFilesInCurrentDir(string currentDirPath, TextBox addressBarTextBox, TextBlock statusBarTextBlock, string searchString, ListView navigListView)
        {
            if (String.IsNullOrWhiteSpace(searchString))
                return;

            MakeStatusBarVisible(addressBarTextBox, statusBarTextBlock);

            navigListView.Items.Clear();

            foreach (var obj in _fileProvider.FindConcurrences(currentDirPath, searchString))
            {
                Folder folder = obj as Folder;
                if (folder != null)
                {
                    navigListView.Items.Add(_viewModelBuilder.CreateVmOfFolder(folder));
                }
                File file = obj as File;
                if (file != null)
                {
                    navigListView.Items.Add(_viewModelBuilder.CreateVmOfFile(file));
                }
            }

            statusBarTextBlock.Text = "Matches found:";

        }

        private void MakeAddressBarVisible(TextBox addressBarTextBox, TextBlock statusBarTextBlock)
        {
            statusBarTextBlock.Visibility = Visibility.Collapsed;
            addressBarTextBox.Visibility = Visibility.Visible;
        }

        private void MakeStatusBarVisible(TextBox addressBarTextBox, TextBlock statusBarTextBlock)
        {
            addressBarTextBox.Visibility = Visibility.Collapsed;
            statusBarTextBlock.Visibility = Visibility.Visible;
            statusBarTextBlock.Text = "Searching...";
        }

        #endregion

        #region Methods for ContextMenu

        private void SetPointsOfItemsContextMenu(string statusBar, ContextMenu contextMenu)
        {
            var cutItem = (MenuItem)contextMenu.Items[0];
            var copyItem = (MenuItem)contextMenu.Items[1];
            var pasteItem = (MenuItem)contextMenu.Items[2];
            var deleteItem = (MenuItem)contextMenu.Items[3];
            var renameItem = (MenuItem)contextMenu.Items[4];
            var separatorItem = (Separator)contextMenu.Items[5];
            var properiesItem = (MenuItem)contextMenu.Items[6];

            properiesItem.Visibility = Visibility.Visible;

            if (!String.IsNullOrWhiteSpace(statusBar))
            {
                separatorItem.Visibility = Visibility.Visible;
                cutItem.Visibility = Visibility.Visible;
                copyItem.Visibility = Visibility.Visible;

                pasteItem.Visibility = Visibility.Visible;
                pasteItem.IsEnabled = _fileManager.HasObjectToPaste;

                deleteItem.Visibility = Visibility.Visible;
                renameItem.Visibility = Visibility.Visible;
            }
            else
            {
                cutItem.Visibility = Visibility.Collapsed;
                copyItem.Visibility = Visibility.Collapsed;
                pasteItem.Visibility = Visibility.Collapsed;
                deleteItem.Visibility = Visibility.Collapsed;
                renameItem.Visibility = Visibility.Collapsed;
                separatorItem.Visibility = Visibility.Collapsed;
                pasteItem.IsEnabled = false;
            }
        }

        private void SetPointsOfContextMenu(ContextMenu contextMenu)
        {
            var pasteItem = (MenuItem)contextMenu.Items[0];
            var properiesItem = (MenuItem)contextMenu.Items[2];

            pasteItem.Visibility = Visibility.Visible;
            properiesItem.Visibility = Visibility.Visible;
            pasteItem.IsEnabled = _fileManager.HasObjectToPaste;
        }

        private ListView GetListViewFromObjectOfMenuItem(object sender)
        {
            var contextMenu = GetContextMenuByObjectSender(sender);
            if (contextMenu == null)
                return null;

            var listView = contextMenu.PlacementTarget as ListView;

            return listView;
        }

        private ObjectInFileSystemViewModel GetViewModelFromObjectOfMenuItem(object sender)
        {
            var contextMenu = GetContextMenuByObjectSender(sender);
            if (contextMenu == null)
                return null;

            var selectedItem = GetViewModelFromObjectOfListVeiewItem(contextMenu.PlacementTarget);

            return selectedItem;
        }

        private ObjectInFileSystemViewModel GetViewModelFromObjectOfListVeiewItem(object sender)
        {
            var listViewItem = sender as ListViewItem;
            if (listViewItem == null)
                return null;

            var selectedItem = listViewItem.Content as ObjectInFileSystemViewModel;

            return selectedItem;
        }

        private ContextMenu GetContextMenuByObjectSender(object sender)
        {
            var menuItem = sender as MenuItem;
            if (menuItem == null)
                return null;

            var contextMenu = menuItem.Parent as ContextMenu;

            return contextMenu;
        }

        private WindowPosition GetPositionFromListview(ListView listView)
        {
            if (listView == LeftFolderAndFileListView)
                return WindowPosition.Left;
            else if (listView == RightFolderAndFileListView)
                return WindowPosition.Right;
            throw new Exception(InternalErrorMessage);
        }

        private WindowPosition ResolveObjectAndGetPosition(object sender)
        {
            var item = GetViewModelFromObjectOfMenuItem(sender);
            if (item != null)
                return item.Position;

            var listView = GetListViewFromObjectOfMenuItem(sender);
            if (listView != null)
                return GetPositionFromListview(listView);

            throw new Exception(InternalErrorMessage);
        }

        #endregion

        #region Methods for Select, Copy, Paste, Delete, Rename

        private void SelectItemInListView(ObjectInFileSystemViewModel selectedItem, bool cut)
        {
            try
            {
                if (selectedItem is FolderViewModel)
                {
                    var item = (FolderViewModel)selectedItem;
                    _fileManager.CopyItem(_fileProvider.GetFolderByPath(item.Path), cut);
                }
                if (selectedItem is FileViewModel)
                {
                    var item = (FileViewModel)selectedItem;
                    _fileManager.CopyItem(_fileProvider.GetFileByPath(item.Path), cut);
                }
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
            }
        }

        private void PasteItem(string destinationPath)
        {
            if (!_fileManager.HasObjectToPaste)
                return;

            if (String.IsNullOrEmpty(destinationPath))
            {
                MessageBox.Show("Cannot past to drive list :)", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!_fileManager.EnsurePathIsNotEqualsToCopiedItemPath(destinationPath))
                return;

            try
            {
                if (!_fileManager.EnsureNoConflictsCopiedItemWithExistingFiles(destinationPath))
                {
                    bool replace = false;

                    MessageBoxResult result = MessageBox.Show("File(s) what you try paste, already exist. \n Do you want replase it?", "Warning", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            replace = true;
                            break;
                        case MessageBoxResult.No:
                            break;
                        case MessageBoxResult.Cancel:
                            return;
                    }

                    ShowMessageExFromList(_fileManager.PasteItem(destinationPath, replace));
                    return;
                }
                else
                    ShowMessageExFromList(_fileManager.PasteItem(destinationPath, true));
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
            }
        }

        private void DeleteItem(object selectedItem)
        {
            var item = selectedItem as ObjectInFileSystemViewModel;
            if (item == null)
                return;

            AskForDeleteItem(item);
        }

        private void AskForDeleteItem(ObjectInFileSystemViewModel item)
        {
            MessageBoxResult result = MessageBox.Show(String.Format("Are you sure you want to delete:\n {0}", item.Path), "Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
                CheckAndDeleteItemInternal(item);
            else
                return;
        }

        private void CheckAndDeleteItemInternal(ObjectInFileSystemViewModel item)
        {
            try
            {
                if (item is FolderViewModel)
                {
                    if (!_fileProvider.CheckValidPathToDir(item.Path))
                    {
                        MessageBox.Show("Current directory is not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    _fileManager.DeleteFolder(item.Path);
                }
                if (item is FileViewModel)
                {
                    if (!_fileProvider.IsFileExist(item.Path))
                    {
                        MessageBox.Show("Current file is not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    _fileManager.DeleteFile(item.Path);
                }
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
            }
        }

        private void RenameItem(object selectedItem)
        {
            var item = selectedItem as ObjectInFileSystemViewModel;

            if (item != null)
                AskForRenameItem(item);
        }

        private void AskForRenameItem(ObjectInFileSystemViewModel item)
        {
            var result = InputBox.Show("Insert name :", item.Name, "Rename", this);

            switch (result.ClickedButton)
            {
                case InputBoxResult.Ok:
                    break;
                default:
                    return;
            }

            CheckCorrectNewNameAndRenameItem(item, result.UserInput);
        }

        private void CheckCorrectNewNameAndRenameItem(ObjectInFileSystemViewModel item, string newName)
        {
            if (item.Name == newName)
                return;

            string distPath = _fileProvider.CombinePathWithName(item.Parent, newName);

            if (_fileProvider.CheckValidPathToDir(distPath) || _fileProvider.IsFileExist(distPath))
            {
                MessageBoxResult choise = MessageBox.Show(String.Format("Object with this name:\n {0} just exist.\n Please< try again", newName), "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                switch (choise)
                {
                    case MessageBoxResult.OK:
                        AskForRenameItem(item);
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                }
                return;
            }

            try
            {
                _fileManager.Rename(item.Path, distPath);
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
            }
        }

        #endregion

        #region Methods for Properties

        private void ShowProperiesOfFile(FileViewModel file)
        {
            FilePropertiesDto fileDto = new FilePropertiesDto
            {
                Name = file.Name,
                Path = file.Path,
                Type = file.Type,
                DateOfCreate = file.DateOfCreate,
                DateOfChange = file.DateOfChange,
                DateOfLastAccess = file.DateOfLastAccess,
                Size = file.Size,
                Atributes = file.Atributes,
                PathToIcon = file.PathToIcon
            };

            var properties = new PropertiesWindow(fileDto);
            properties.Show();
        }

        private void ShowProperiesOfFolder(FolderViewModel folder)
        {
            long size = 0;

            try
            {
                size = _fileProvider.GetSizeOfFolder(folder.Path);
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
                return;
            }

            var folderDto = new FolderPropertiesDto
            {
                Name = folder.Name,
                Path = folder.Path,
                DateOfCreate = folder.DateOfCreate,
                DateOfChange = folder.DateOfChange,
                DateOfLastAccess = folder.DateOfLastAccess,
                Size = size,
                Atributes = folder.Atributes,
                PathToIcon = folder.PathToIcon
            };

            var properties = new PropertiesWindow(folderDto);
            properties.Show();
        }

        private void ShowPropertiesOfDrive(DriveViewModel drive)
        {
            var driveDto = new DrivePropertiesDto
            {
                Name = drive.Name,
                Path = drive.Path,
                TypeOfDrive = drive.TypeOfDrive,
                FormatOfDrive = drive.FormatOfDrive,
                TotalSize = drive.TotalSize,
                FreeSpace = drive.FreeSpace,
                BusySpace = drive.BusySpace,
                PathToIcon = drive.PathToIcon
            };

            var properties = new PropertiesWindow(driveDto);
            properties.Show();
        }

        private void ShowPropertiesOfSelectedItem(object selectedItem, string path)
        {
            if (String.IsNullOrWhiteSpace(path))
            {
                DriveViewModel drive = (DriveViewModel)selectedItem;
                ShowPropertiesOfDrive(drive);
            }
            else if (selectedItem is FolderViewModel)
            {
                FolderViewModel folder = (FolderViewModel)selectedItem;
                ShowProperiesOfFolder(folder);
            }
            else if (selectedItem is FileViewModel)
            {
                FileViewModel file = (FileViewModel)selectedItem;
                ShowProperiesOfFile(file);
            }
        }

        private void ShowPropertiesOfCurrentDir(string path, Label container)
        {
            bool isDrive = true;

            try
            {
                isDrive = _fileProvider.CheckIsDrive(path);
            }
            catch (Exception ex)
            {
                ShowMessageByEx(ex);
                return;
            }

            if (isDrive)
            {
                Drive drive = container.Content as Drive;
                if (drive == null)
                    return;

                ShowPropertiesOfDrive(_viewModelBuilder.CreateVmOfDrive(drive));
            }
            else if (!String.IsNullOrWhiteSpace(path))
            {
                Folder folder = container.Content as Folder;
                if (folder == null)
                    return;

                ShowProperiesOfFolder(_viewModelBuilder.CreateVmOfFolder(folder));
            }
        }

        private void ShowProperties(ObjectInFileSystemViewModel item, string path, Label container)
        {
            if (item == null && container != null)
            {
                ShowPropertiesOfCurrentDir(path, container);
            }
            else
            {
                ShowPropertiesOfSelectedItem(item, path);
            }
        }

        #endregion

        #region Methods for DragnDrop

        private ListViewItem ResolveDragListViewItem(MouseEventArgs e)
        {
            var originalSourse = e.OriginalSource as Border;
            if (originalSourse == null)
                return null;

            ListViewItem listViewItem = originalSourse.TemplatedParent as ListViewItem;
            return listViewItem;
        }

        private DragDropEffects ResolveMoveDragEffects(object sender, DragEventArgs e, string addressBarText)
        {
            if (!e.Data.GetDataPresent(FormatForDataObject) || sender == e.Source || String.IsNullOrWhiteSpace(addressBarText))
                return DragDropEffects.None;
            return DragDropEffects.Move;
        }

        private void DoDragItem(object sender, MouseEventArgs e, string addressBarText)
        {
            if (String.IsNullOrWhiteSpace(addressBarText))
                return;

            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                var listViewItem = ResolveDragListViewItem(e);
                if (listViewItem == null)
                    return;

                var item = listViewItem.Content;

                DataObject dragData = new DataObject(FormatForDataObject, item);
                DragDrop.DoDragDrop(listViewItem, dragData, DragDropEffects.Move);
            }
        }

        private void DoDropItem(object sender, DragEventArgs e, string addressBarText)
        {
            if (e.Data.GetDataPresent(FormatForDataObject) && !String.IsNullOrWhiteSpace(addressBarText))
            {
                ObjectInFileSystemViewModel item = e.Data.GetData(FormatForDataObject) as ObjectInFileSystemViewModel;
                if (item == null)
                    return;

                MoveItemByDragDrop(addressBarText, item);
            }
        }

        private void MoveItemByDragDrop(string path, ObjectInFileSystemViewModel item)
        {
            SelectItemInListView(item, true);
            PasteItem(path);
            UpdateListViews();
        }

        #endregion

        #region Methods for Messages

        private void ShowMessageByEx(Exception ex)
        {
            string message = String.Format(" File System Error: \n {0}", ex.Message);
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ShowMessageExFromList(List<Exception> errorList)
        {
            if (errorList == null || errorList.Count == 0)
                return;

            var generalErrorMessage = new StringBuilder();

            foreach (Exception ex in errorList)
            {
                generalErrorMessage.AppendLine(ex.Message);
                generalErrorMessage.AppendLine();
            }

            ShowMessageByEx(new Exception(generalErrorMessage.ToString()));
        }
        #endregion
    }
}

