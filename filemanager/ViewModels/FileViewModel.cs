﻿using System;

namespace filemanager.ViewModels
{
    public class FileViewModel : ObjectInFileSystemViewModel
    {
        private const string pathToIcon = @"Images\File_icon.png";

        public string Type { get; set; }
        public DateTime DateOfCreate { get; set; }
        public DateTime DateOfChange { get; set; }
        public DateTime DateOfLastAccess { get; set; }
        public long Size { get; set; }
        public string Atributes { get; set; }

        public override string PathToIcon
        {
            get
            {
                return pathToIcon;
            }
        }
    }
}
