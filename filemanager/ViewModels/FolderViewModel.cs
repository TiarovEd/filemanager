﻿using System;

namespace filemanager.ViewModels
{
    public class FolderViewModel : ObjectInFileSystemViewModel
    {
        private const string pathToIcon = @"Images\Folder_icon.png";

        public DateTime DateOfCreate { get; set; }
        public DateTime DateOfChange { get; set; }
        public DateTime DateOfLastAccess { get; set; }
        public string Atributes { get; set; }

        public override string PathToIcon
        {
            get
            {
                return pathToIcon;
            }
        }
    }
}
