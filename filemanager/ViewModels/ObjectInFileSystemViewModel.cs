﻿

namespace filemanager.ViewModels
{
    public abstract class ObjectInFileSystemViewModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Parent { get; set; }
        public abstract string PathToIcon { get; }
        public WindowPosition Position { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
