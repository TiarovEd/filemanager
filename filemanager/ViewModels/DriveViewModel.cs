﻿

namespace filemanager.ViewModels
{
    public class DriveViewModel : ObjectInFileSystemViewModel
    {
        private const string pathToIcon = @"Images\Driver_icon.png";

        public string TypeOfDrive { get; set; }
        public string FormatOfDrive { get; set; }
        public long TotalSize { get; set; }
        public long FreeSpace { get; set; }
        public long BusySpace { get; set; }

        public override string PathToIcon
        {
            get
            {
                return pathToIcon;
            }
        }
    }
}
