﻿using filemanager.Models;

namespace filemanager.ViewModels
{
    public class VmBuilder
    {
        public FileViewModel CreateVmOfFile(File model)
        {
            var viewModel = new FileViewModel
            {
                Name = model.Name,
                Path = model.Path,
                Parent = model.Parent,
                Type = model.Type,
                DateOfCreate = model.DateOfCreate,
                DateOfChange = model.DateOfChange,
                DateOfLastAccess = model.DateOfLastAccess,
                Size = model.Size,
                Atributes = model.Atributes
            };
            return viewModel;
        }

        public FolderViewModel CreateVmOfFolder(Folder model)
        {
            var viewModel = new FolderViewModel
            {
                Name = model.Name,
                Path = model.Path,
                Parent = model.Parent,
                DateOfCreate = model.DateOfCreate,
                DateOfChange = model.DateOfChange,
                DateOfLastAccess = model.DateOfLastAccess,
                Atributes = model.Atributes
            };
            return viewModel;
        }

        public DriveViewModel CreateVmOfDrive(Drive model)
        {
            var viewModel = new DriveViewModel
            {
                Name = model.Name,
                Path = model.Path,
                Parent = model.Parent,
                TypeOfDrive = model.TypeOfDrive,
                FormatOfDrive = model.FormatOfDrive,
                TotalSize = model.TotalSize,
                FreeSpace = model.FreeSpace,
                BusySpace = model.BusySpace
            };
            return viewModel;
        }
    }
}
