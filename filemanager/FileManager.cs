﻿using System;
using System.IO;
using filemanager.Models;
using filemanager.DtoModels;
using System.Windows;
using System.Collections.Generic;
using System.Security.AccessControl;

using FileInFileSystem = filemanager.Models.File;

namespace filemanager
{
    public class FileManager
    {
        #region Error messages

        private const string DirNotExistsErrorMessageFormat = "Directory: {0} is not exists";
        private const string NoPermissionErrorMessageFormat = "No permissions to acess \na {0}: {1}";
        private const string IOStreamDeleteErrorMessageFormat = "Removal: {0} \n is not possible because \n the process is not completed";
        private const string NameTooLongErrorMessageFormat = "Name is too long - path should contain less than \n 248 characters. Current length of path with new name: {0}";
        private const string NoPermissionToRenameErrorMessageFormat = "No permissions to rename";
        private const string CannotContainCharErrorMessageFormat = "A filename cannot contain any of the \n following characters: \\ / : * ? \" < > | ";

        private const string DeleteErrorMessageFormat = "Could not delete {0}: \n {1}";
        private const string PasteErrorMessageFormat = "Unknown error occured while trying copying {0}: \n{1}";
        private const string UnknownErrorWhilePasteMessageFormat = "Fatal error while paste object!";
        private const string RenameErrorMessageFormat = "Could not rename";

        #endregion

        private readonly FileProvider _fileProvider = new FileProvider();

        #region Properties and methods for properties

        public bool HasObjectToPaste
        {
            get
            {
                return (ObjectInBuffer != null);
            }
        }

        private BufferItemDto ObjectInBuffer
        {
            get
            {
                BufferItemDto obj = GetObjectFromClipboard();
                if (obj != null && !_fileProvider.IsFileExist(obj.Item.Path) && !_fileProvider.CheckValidPathToDir(obj.Item.Path))
                {
                    Clipboard.Clear();
                    return null;
                }
                return obj;
            }
        }

        private BufferItemDto GetObjectFromClipboard()
        {
            var obj = Clipboard.GetDataObject();

            if (obj == null)
                return null;

            BufferItemDto objectInBuffer = obj.GetData(typeof(BufferItemDto)) as BufferItemDto;

            return objectInBuffer;
        }

        #endregion

        #region public methods Rename, Copy, Cut, Paste ,Delete

        public bool EnsurePathIsNotEqualsToCopiedItemPath(string path)
        {
            if (ObjectInBuffer.Item.Parent == path)
                return false;
            return true;
        }

        public bool EnsureNoConflictsCopiedItemWithExistingFiles(string path)
        {
            var item = ObjectInBuffer.Item;
            string distPath = Path.Combine(path, item.Name);

            if (_fileProvider.IsFileExist(distPath))
            {
                return false;
            }

            if (_fileProvider.CheckValidPathToDir(distPath))
            {
                return false;
            }

            return true;
        }

        public void Rename(string soursePath, string newName)
        {
            string distPath = _fileProvider.CombinePathWithName(Path.GetDirectoryName(soursePath), newName);

            try
            {
                Directory.Move(soursePath, distPath);
            }

            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(NameTooLongErrorMessageFormat, distPath.Length), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(NoPermissionToRenameErrorMessageFormat, ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, _fileProvider.GetParentByPath(soursePath).Path), ex);
            }
            catch (ArgumentException ex)
            {
                throw new Exception(CannotContainCharErrorMessageFormat, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(RenameErrorMessageFormat, ex);
            }
        }

        public void CopyItem(ObjectInFileSystem item, bool cut)
        {
            BufferItemDto bufObj = new BufferItemDto(item, cut);
            Clipboard.Clear();
            Clipboard.SetDataObject(bufObj);
        }

        public List<Exception> PasteItem(string path, bool replace)
        {
            var item = ObjectInBuffer.Item;

            if (item is FileInFileSystem && replace)
            {
                PasteFile(path);
            }
            else if (item is Folder)
            {
                var errorList = PasteDirectory(path, replace);
                return errorList;
            }
            else
                throw new Exception(UnknownErrorWhilePasteMessageFormat);

            return null;
        }

        public void DeleteFile(string path)
        {
            try
            {
                System.IO.File.Delete(path);
            }
            catch (IOException ex)
            {
                throw new Exception(String.Format(IOStreamDeleteErrorMessageFormat, path), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionErrorMessageFormat, FileSystemEntityType.File, path), ex);

            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(DeleteErrorMessageFormat, FileSystemEntityType.File, path), ex);
            }
        }

        public void DeleteFolder(string path)
        {
            try
            {
                Directory.Delete(path, true);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, path), ex);
            }
            catch (IOException ex)
            {
                throw new Exception(IOStreamDeleteErrorMessageFormat, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionErrorMessageFormat, FileSystemEntityType.Folder, path), ex);

            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(DeleteErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
        }

        #endregion

        #region private methods

        private List<Exception> PasteDirectory(string path, bool replace)
        {
            var item = ObjectInBuffer.Item;
            Folder folder = item as Folder;
            if (folder == null)
                throw new Exception("You are trying to past folder, but there are not folder in buffer");

            bool cut = ObjectInBuffer.Cut;

            List<Exception> errorList = new List<Exception>();

            PasteFolder(folder, path, replace, errorList);

            if (cut)
            {
                string soursePath = folder.Path;

                try
                {
                    DeleteFolder(soursePath);
                }
                catch (Exception ex)
                {
                    errorList.Add(ex);
                }
            }

            return errorList;
        }

        private void PasteFolder(Folder folder, string path, bool replace, List<Exception> errorList)
        {
            if (path == null || folder.Name == null)
                throw new Exception(String.Format(PasteErrorMessageFormat, FileSystemEntityType.Folder, folder.Path));

            if (HasInvalidChar(path) || HasInvalidChar(folder.Name))
                throw new Exception(CannotContainCharErrorMessageFormat);

            string destPath = Path.Combine(path, folder.Name);

            if (!HasReadPermissions(folder.Path))
                throw new Exception(String.Format(NoPermissionErrorMessageFormat, FileSystemEntityType.Folder, folder.Path));

            if (Directory.Exists(destPath) && !HasWritePermissions(destPath))
                throw new Exception(String.Format(NoPermissionErrorMessageFormat, FileSystemEntityType.Folder, destPath));
            else if (!HasWritePermissions(path))
                throw new Exception(String.Format(NoPermissionErrorMessageFormat, FileSystemEntityType.Folder, path));

            try
            {
                PasteFolderInternal(folder.Path, destPath, replace, errorList);
            }
            catch (Exception ex)
            {
                errorList.Add(new Exception(String.Format(PasteErrorMessageFormat, FileSystemEntityType.Folder, folder.Path), ex));
            }

        }

        private void PasteFolderInternal(string soursePath, string destPath, bool replace, List<Exception> errorList)
        {
            DirectoryInfo dir = new DirectoryInfo(soursePath);
            DirectoryInfo[] dirs = dir.GetDirectories();
            FileInfo[] files = dir.GetFiles();

            if (!Directory.Exists(destPath))
            {
                Directory.CreateDirectory(destPath);
            }

            foreach (FileInfo file in files)
            {
                try
                {
                    string temppath = Path.Combine(destPath, file.Name);

                    if (System.IO.File.Exists(temppath) && replace)
                    {
                        DeleteFile(temppath);
                        file.CopyTo(temppath, false);
                    }
                    else if (!System.IO.File.Exists(temppath))
                        file.CopyTo(temppath, false);
                }
                catch (Exception ex)
                {
                    errorList.Add(new Exception(String.Format(PasteErrorMessageFormat, FileSystemEntityType.File, file.FullName), ex));
                }
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                try
                {
                    string temppath = Path.Combine(destPath, subdir.Name);
                    PasteFolderInternal(subdir.FullName, temppath, replace, errorList);
                }
                catch (Exception ex)
                {
                    errorList.Add(new Exception(String.Format(PasteErrorMessageFormat, FileSystemEntityType.Folder, subdir.FullName), ex));
                }
            }
        }

        private void PasteFile(string path)
        {
            var item = ObjectInBuffer.Item;
            FileInFileSystem file = item as FileInFileSystem;
            bool cut = ObjectInBuffer.Cut;

            if (cut)
                MoveFile(file, path);
            else
                PasteFile(file, path);
        }

        private void PasteFile(FileInFileSystem file, string path)
        {
            try
            {
                string distPath = Path.Combine(path, file.Name);

                if (_fileProvider.IsFileExist(distPath))
                    DeleteFile(distPath);
                System.IO.File.Copy(file.Path, distPath);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(PasteErrorMessageFormat, FileSystemEntityType.File, file.Name), ex);
            }
        }

        private void MoveFile(FileInFileSystem file, string path)
        {
            try
            {
                string distPath = Path.Combine(path, file.Name);

                if (_fileProvider.IsFileExist(distPath))
                    DeleteFile(distPath);

                System.IO.File.Move(file.Path, distPath);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(PasteErrorMessageFormat, FileSystemEntityType.File, file.Name), ex);
            }
        }

        private bool HasReadPermissions(string path)
        {
            try
            {
                var accessControlLisе = Directory.GetAccessControl(path);
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }

            return true;
        }

        private bool HasWritePermissions(string path)
        {
            if (!HasReadPermissions(path))
                return false;

            var writeAllow = false;
            var writeDeny = false;
            var accessControlList = Directory.GetAccessControl(path);
            if (accessControlList == null)
                return false;
            var accessRules = accessControlList.GetAccessRules(true, true,
                                        typeof(System.Security.Principal.SecurityIdentifier));
            if (accessRules == null)
                return false;

            foreach (FileSystemAccessRule rule in accessRules)
            {
                if ((FileSystemRights.Write & rule.FileSystemRights) != FileSystemRights.Write)
                    continue;

                if (rule.AccessControlType == AccessControlType.Allow)
                    writeAllow = true;
                else if (rule.AccessControlType == AccessControlType.Deny)
                    writeDeny = true;
            }

            return writeAllow && !writeDeny;
        }

        private bool HasInvalidChar(string path)
        {
            foreach (char invalidCharacter in Path.GetInvalidPathChars())
            {
                if (path.Contains(invalidCharacter.ToString()))
                    return true;
            }
            return false;
        }

        #endregion
    }
}
