﻿using System;

namespace filemanager.Models
{
    [Serializable]
    public class ObjectInFileSystem
    {
        public ObjectInFileSystem(string name, string path, string parent)
        {
            Name = name;
            Path = path;
            Parent = parent;
        }

        public string Name { get; private set; }
        public string Path { get; private set; }
        public string Parent { get; private set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
