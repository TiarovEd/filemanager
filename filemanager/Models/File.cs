﻿using System;

namespace filemanager.Models
{
    [Serializable]
    public class File : ObjectInFileSystem
    {
        public File(string name, string path, string parent, string type,
            DateTime dateOfCreate, DateTime dateOfChange, DateTime dateOfLastAccess,
            long size, string atributes) : base(name, path, parent)
        {
            Type = type;
            DateOfCreate = dateOfCreate;
            DateOfChange = dateOfChange;
            DateOfLastAccess = dateOfLastAccess;
            Size = size;
            Atributes = atributes;
        }

        public string Type { get; private set; }
        public DateTime DateOfCreate { get; private set; }
        public DateTime DateOfChange { get; private set; }
        public DateTime DateOfLastAccess { get; private set; }
        public long Size { get; private set; }
        public string Atributes { get; private set; }

    }
}
