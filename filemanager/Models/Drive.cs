﻿

namespace filemanager.Models
{
    public class Drive : ObjectInFileSystem
    {
        public Drive(string name, string path,
            string typeOfDrive, string formatOfDrive,
            long totalSize, long freeSpace, long busySpace) : base(name, path, null)
        {
            TypeOfDrive = typeOfDrive;
            FormatOfDrive = formatOfDrive;
            TotalSize = totalSize;
            FreeSpace = freeSpace;
            BusySpace = busySpace;
        }

        public string TypeOfDrive { get; private set; }
        public string FormatOfDrive { get; private set; }
        public long TotalSize { get; private set; }
        public long FreeSpace { get; private set; }
        public long BusySpace { get; private set; }

    }
}
