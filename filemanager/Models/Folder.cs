﻿using System;

namespace filemanager.Models
{
    [Serializable]
    public class Folder : ObjectInFileSystem
    {
        public Folder(string name, string path, string parent,
            DateTime dateOfCreate, DateTime dateOfChange, DateTime dateOfLastAccess, string atributes) : base(name, path, parent)
        {
            DateOfCreate = dateOfCreate;
            DateOfChange = dateOfChange;
            DateOfLastAccess = dateOfLastAccess;
            Atributes = atributes;
        }

        public DateTime DateOfCreate { get; private set; }
        public DateTime DateOfChange { get; private set; }
        public DateTime DateOfLastAccess { get; private set; }
        public string Atributes { get; private set; }

    }
}
