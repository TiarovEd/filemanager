﻿
using System.Windows;
using System.Windows.Input;

namespace filemanager
{
    /// <summary>
    /// Interaction logic for InputDialogWindow.xaml
    /// </summary>
    public partial class InputDialogWindow : Window
    {
        private InputBoxResult _clickedButton = InputBoxResult.None;

        public InputDialogWindow()
        {
            InitializeComponent();
        }

        public InputDialogResult ShowInputDialog(string message, string defaultInput, string title)
        {
            _clickedButton = InputBoxResult.None;
            MesssageToUserTextBlock.Text = message;
            InputTextBox.Text = defaultInput;
            this.Title = title;
            this.ShowDialog();

            return new InputDialogResult(_clickedButton, InputTextBox.Text);
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            ClickedOkAndCloseWindow();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _clickedButton = InputBoxResult.Cancel;
            this.Close();
        }

        private void InputTextBox_KeyEnterDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                ClickedOkAndCloseWindow();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void ClickedOkAndCloseWindow()
        {
            _clickedButton = InputBoxResult.Ok;
            this.Close();
        }
    }
}
