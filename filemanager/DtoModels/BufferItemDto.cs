﻿using System;
using filemanager.Models;

namespace filemanager.DtoModels
{
    [Serializable]
    public class BufferItemDto 
    {
        public BufferItemDto(ObjectInFileSystem item, bool cut)
        {
            Item = item;
            Cut = cut;
        }

        public ObjectInFileSystem Item { get; private set; }
        public bool Cut { get; private set; }
    }
}
