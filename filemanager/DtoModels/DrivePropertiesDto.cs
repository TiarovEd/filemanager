﻿

namespace filemanager.DtoModels
{
    public class DrivePropertiesDto : ObjInFileSystemPropertiesDto
    {
        public string TypeOfDrive { get; set; }
        public string FormatOfDrive { get; set; }
        public long TotalSize { get; set; }
        public long FreeSpace { get; set; }
        public long BusySpace { get; set; }
    }
}
