﻿

namespace filemanager.DtoModels
{
    public abstract class ObjInFileSystemPropertiesDto
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string PathToIcon { get; set; }
    }
}
