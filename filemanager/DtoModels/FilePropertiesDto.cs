﻿using System;

namespace filemanager.DtoModels
{
    public class FilePropertiesDto : ObjInFileSystemPropertiesDto
    {
        public string Type { get; set; }
        public DateTime DateOfCreate { get; set; }
        public DateTime DateOfChange { get; set; }
        public DateTime DateOfLastAccess { get; set; }
        public long Size { get; set; }
        public string Atributes { get; set; }
    }
}
