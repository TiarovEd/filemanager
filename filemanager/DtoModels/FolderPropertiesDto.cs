﻿using System;

namespace filemanager.DtoModels
{
    public class FolderPropertiesDto : ObjInFileSystemPropertiesDto
    {
        public DateTime DateOfCreate { get; set; }
        public DateTime DateOfChange { get; set; }
        public DateTime DateOfLastAccess { get; set; }
        public long Size { get; set; }
        public string Atributes { get; set; }
    }
}
