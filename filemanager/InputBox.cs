﻿using System.Windows;

namespace filemanager
{
    public static class InputBox
    {
        private static readonly InputDialogWindow _inputWindow = new InputDialogWindow();

        public static InputDialogResult Show(string message, string defaultInput, string title, Window owner)
        {
            AssignOwner(owner);
            return _inputWindow.ShowInputDialog(message, defaultInput, title);
        }

        private static void AssignOwner(Window owner)
        {
            _inputWindow.Owner = owner;
        }
    }
}
