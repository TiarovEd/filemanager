﻿using System.IO;
using System;
using System.Collections.Generic;
using filemanager.Models;
using System.Security;

using FileInFileSystem = filemanager.Models.File;

namespace filemanager
{
    public class FileProvider
    {
        #region error messages

        private const string NotValidErrorMessageFormat = "Not valid Path: {0}";
        private const string PathTooLongErrorMessageFormat = "Path is too long - it should contain less than 248 characters. Current length: {0}";
        private const string DirNotExistsErrorMessageFormat = "Directory: {0} is not exists";
        private const string NoPermissionToOpenErrorMessageFormat = "No permissions to open a {0}: {1}";
        private const string NoPermissionGetDrivesErrorMessageFormat = "No permissions to get drives";
        private const string ErrorReadErrorMessageFormat = "Error while trying to access read";
        private const string UnknownErrorToOpenDirErrorMessageFormat = "Unknown error occured while trying to open {0}";
        private const string UnknownErrorOfGetDrivesErrorMessageFormat = "Unknown error occured while trying get drives";
        private const string UnknownErrorOfGetDriveErrorMessageFormat = "Unknown error occured while trying get drive";

        #endregion

        #region get Lists<>

        public List<Drive> GetListOfDrives()
        {
            DriveInfo[] allDrives;
            List<Drive> listOfDrives = new List<Drive>();

            try
            {
                allDrives = DriveInfo.GetDrives();
            }
            catch (IOException ex)
            {
                throw new Exception(ErrorReadErrorMessageFormat, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(NoPermissionGetDrivesErrorMessageFormat, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(UnknownErrorOfGetDrivesErrorMessageFormat, ex);
            }

            foreach (DriveInfo drive in allDrives)
            {
                if (drive.IsReady)
                    listOfDrives.Add(BuildDrive(drive));
            }

            return listOfDrives;
        }

        public List<Folder> GetListOfFolders(string pathOfDir)
        {
            DirectoryInfo currentFolder;
            DirectoryInfo[] folders;

            try
            {
                currentFolder = Directory.CreateDirectory(pathOfDir);
                folders = currentFolder.GetDirectories();
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, pathOfDir), ex);
            }
            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(PathTooLongErrorMessageFormat, pathOfDir), ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, pathOfDir), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, pathOfDir), ex);
            }
            catch (SecurityException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, pathOfDir), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(UnknownErrorToOpenDirErrorMessageFormat, pathOfDir), ex);
            }

            var showList = new List<Folder>();

            foreach (var folder in folders)
            {
                showList.Add(BuildFolder(folder));
            }

            return showList;
        }

        public List<FileInFileSystem> GetListOfFiles(string pathOfDir)
        {
            DirectoryInfo currentFolder;
            FileInfo[] files;

            try
            {
                currentFolder = Directory.CreateDirectory(pathOfDir);
                files = currentFolder.GetFiles();
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, pathOfDir), ex);
            }
            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(PathTooLongErrorMessageFormat, pathOfDir), ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, pathOfDir), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.File, pathOfDir), ex);
            }
            catch (SecurityException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.File, pathOfDir), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(UnknownErrorToOpenDirErrorMessageFormat, pathOfDir), ex);
            }

            var showList = new List<FileInFileSystem>();

            foreach (var file in files)
            {
                showList.Add(BuildFile(file));
            }

            return showList;
        }

        public List<ObjectInFileSystem> FindConcurrences(string path, string searchPattern = "*.*")
        {
            List<ObjectInFileSystem> list = null;

            try
            {
                list = FindConcurrencesInternal(path, searchPattern);
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, path), ex);
            }
            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(PathTooLongErrorMessageFormat, path), ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, path), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (SecurityException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(UnknownErrorToOpenDirErrorMessageFormat, path), ex);
            }

            return list;
        }

        #endregion

        #region get Objects

        public ObjectInFileSystem GetParentByPath(string path)
        {
            DirectoryInfo folder;

            try
            {
                folder = Directory.GetParent(path);
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, path), ex);
            }
            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(PathTooLongErrorMessageFormat, path), ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, path), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (SecurityException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(UnknownErrorToOpenDirErrorMessageFormat, path), ex);
            }
            if (!CheckIsDrive(folder.Name))
                return BuildFolder(folder);
            else
                return GetDriveByPath(folder.Name);

        }

        public FileInFileSystem GetFileByPath(string path)
        {
            FileInfo file = null;

            try
            {
                file = new FileInfo(path);
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, path), ex);
            }
            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(PathTooLongErrorMessageFormat, path), ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, path), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.File, path), ex);
            }
            catch (SecurityException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.File, path), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(UnknownErrorToOpenDirErrorMessageFormat, path), ex);
            }

            return BuildFile(file);
        }

        public Folder GetFolderByPath(string path)
        {
            DirectoryInfo dir = null;

            try
            {
                dir = Directory.CreateDirectory(path);
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, path), ex);
            }
            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(PathTooLongErrorMessageFormat, path), ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, path), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (SecurityException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(UnknownErrorToOpenDirErrorMessageFormat, path), ex);
            }

            return BuildFolder(dir);
        }

        public Drive GetDriveByPath(string path)
        {
            DriveInfo drive = null;

            try
            {
                drive = new DriveInfo(path);
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, path), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(UnknownErrorOfGetDriveErrorMessageFormat, ex);
            }

            return BuildDrive(drive);
        }

        #endregion

        #region service methods 

        public string CombinePathWithName(string path, string name)
        {
            return Path.Combine(path, name);
        }

        public bool CheckIsDrive(string path)
        {
            List<Drive> list = GetListOfDrives();

            foreach (var drive in list)
            {
                if (path == drive.Path)
                    return true;
            }
            return false;
        }

        public bool CheckValidPathToDir(string path)
        {
            return Directory.Exists(path);
        }

        public bool IsFileExist(string path)
        {
            return System.IO.File.Exists(path);
        }

        public bool TryOpenFileByPath(string path)
        {
            bool valid = System.IO.File.Exists(path);
            if (valid)
            {
                try
                {
                    System.Diagnostics.Process.Start(path);
                }
                catch
                {
                    valid = false;
                }
            }
            return valid;
        }

        public long GetSizeOfFolder(string path)
        {
            long folderSize = 0;

            try
            {
                folderSize = GetSizeOfFolderInternal(path);
            }
            catch (ArgumentException ex)
            {
                throw new Exception(String.Format(NotValidErrorMessageFormat, path), ex);
            }
            catch (PathTooLongException ex)
            {
                throw new Exception(String.Format(PathTooLongErrorMessageFormat, path), ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new Exception(String.Format(DirNotExistsErrorMessageFormat, path), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (SecurityException ex)
            {
                throw new Exception(String.Format(NoPermissionToOpenErrorMessageFormat, FileSystemEntityType.Folder, path), ex);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format(UnknownErrorToOpenDirErrorMessageFormat, path), ex);
            }

            return folderSize;
        }

        #endregion

        #region private methods

        private List<ObjectInFileSystem> FindConcurrencesInternal(string path, string searchPattern = "*.*")
        {
            var list = new List<ObjectInFileSystem>();

            DirectoryInfo dir = Directory.CreateDirectory(path);
            DirectoryInfo[] allFolders = dir.GetDirectories();

            DirectoryInfo[] folders = dir.GetDirectories(ResolveSearchPattern(searchPattern));
            FileInfo[] files = dir.GetFiles(ResolveSearchPattern(searchPattern));

            foreach (var folder in folders)
            {
                list.Add(BuildFolder(folder));
            }

            foreach (var file in files)
            {
                list.Add(BuildFile(file));
            }

            foreach (var folder in allFolders)
            {
                try
                {
                    list.AddRange(FindConcurrencesInternal(folder.FullName, searchPattern));
                }
                catch
                {
                }
            }

            return list;
        }

        private Drive BuildDrive(DriveInfo drive)
        {
            return new Drive(
                drive.Name, drive.Name,
                drive.DriveType.ToString(),
                drive.DriveFormat,
                drive.TotalSize,
                drive.TotalFreeSpace,
                GetBusySpaceOfDrive(drive)
                );
        }

        private Folder BuildFolder(DirectoryInfo folder)
        {
            return new Folder(
                folder.Name,
                folder.FullName,
                folder.Parent.FullName,
                folder.CreationTime,
                folder.LastWriteTime,
                folder.LastAccessTime,
                folder.Attributes.ToString()
                );
        }

        private FileInFileSystem BuildFile(FileInfo file)
        {
            return new FileInFileSystem(
                file.Name,
                file.FullName,
                file.Directory.FullName,
                file.Extension,
                file.CreationTime,
                file.LastWriteTime,
                file.LastAccessTime,
                file.Length,
                file.Attributes.ToString()
                );
        }

        private long GetSizeOfFolderInternal(string path)
        {
            long size = 0;
            long folderSize = 0;
            DirectoryInfo dir = Directory.CreateDirectory(path);
            DirectoryInfo[] folders = dir.GetDirectories();

            foreach (var folder in folders)
            {
                try
                {
                    folderSize = GetSizeOfFolderInternal(folder.FullName);
                }
                catch
                {
                    folderSize = 0;
                }
                size += folderSize;
            }

            FileInfo[] files = dir.GetFiles();

            foreach (var file in files)
            {
                size += file.Length;
            }

            return size;
        }

        private long GetBusySpaceOfDrive(DriveInfo drive)
        {
            return drive.TotalSize - drive.TotalFreeSpace;
        }

        private string ResolveSearchPattern(string searchText)
        {
            if (searchText.Contains("*") || searchText.Contains("."))
                return searchText;
            else
                return String.Format("*{0}*.*", searchText);
        }

        #endregion
    }
}
